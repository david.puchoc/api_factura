<?php
namespace App\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\FacturaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Factura;
use App\Form\Type\DocumentoFormType;

class DocumentoController extends AbstractFOSRestController
{

    /**
     * @Rest\Get(path="/documento")
     * @Rest\View(serializerGroups={"factura"}, serializerEnableMaxDepthChecks=true)
     */
    public function getActions(FacturaRepository $facturaRepository) 
    {
        return $facturaRepository->findAll();
    }

    /**
     * @Rest\Post(path="/documento")
     * @Rest\View(serializerGroups={"factura"}, serializerEnableMaxDepthChecks=true)
     */
    public function postActions(EntityManagerInterface $entity, Request $request) 
    {
        $factura = new Factura();
        $form = $this->createForm(DocumentoFormType::class, $factura);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $entity->persist($factura);
            $entity->flush();
            return $factura;
        }
        return $form;
    }
}