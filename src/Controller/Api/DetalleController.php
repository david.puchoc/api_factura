<?php
namespace App\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\DetalleRepository;
use App\Repository\FacturaRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Detalle;
use App\Entity\Factura;
use App\Form\Type\DetalleFormType;

class DetalleController extends AbstractFOSRestController
{

    /**
     * @Rest\Get(path="/detalle")
     * @Rest\View(serializerGroups={"detalle"}, serializerEnableMaxDepthChecks=true)
     */
    public function getActions(DetalleRepository $detalleRepository, Request $request) 
    {
        //return $detalleRepository->findAll();
        return $detalleRepository->findBy(
            //array('factura' => 21),
            array('factura' => $request->get('factura',null)),
          );
        
    }

    /**
     * @Rest\Post(path="/detalle")
     * @Rest\View(serializerGroups={"detalle"}, serializerEnableMaxDepthChecks=true)
     */
    public function postActions(EntityManagerInterface $entity, Request $request, FacturaRepository $facturaRepository) 
    {
        $detalle = new Detalle();
        $form = $this->createForm(DetalleFormType::class, $detalle);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $entity->persist($detalle);
            $entity->flush();
            return $detalle;
        }
        return $form;
    }
}