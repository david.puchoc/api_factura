<?php

namespace ContainerKtNpKxF;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderbb9e4 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerfe8b8 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties3c8e8 = [
        
    ];

    public function getConnection()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getConnection', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getMetadataFactory', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getExpressionBuilder', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'beginTransaction', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getCache', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getCache();
    }

    public function transactional($func)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'transactional', array('func' => $func), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'wrapInTransaction', array('func' => $func), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'commit', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->commit();
    }

    public function rollback()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'rollback', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getClassMetadata', array('className' => $className), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'createQuery', array('dql' => $dql), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'createNamedQuery', array('name' => $name), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'createQueryBuilder', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'flush', array('entity' => $entity), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'clear', array('entityName' => $entityName), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->clear($entityName);
    }

    public function close()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'close', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->close();
    }

    public function persist($entity)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'persist', array('entity' => $entity), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'remove', array('entity' => $entity), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'refresh', array('entity' => $entity), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'detach', array('entity' => $entity), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'merge', array('entity' => $entity), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getRepository', array('entityName' => $entityName), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'contains', array('entity' => $entity), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getEventManager', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getConfiguration', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'isOpen', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getUnitOfWork', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getProxyFactory', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'initializeObject', array('obj' => $obj), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'getFilters', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'isFiltersStateClean', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'hasFilters', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return $this->valueHolderbb9e4->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerfe8b8 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderbb9e4) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderbb9e4 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderbb9e4->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, '__get', ['name' => $name], $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        if (isset(self::$publicProperties3c8e8[$name])) {
            return $this->valueHolderbb9e4->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderbb9e4;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderbb9e4;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderbb9e4;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderbb9e4;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, '__isset', array('name' => $name), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderbb9e4;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderbb9e4;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, '__unset', array('name' => $name), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderbb9e4;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderbb9e4;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, '__clone', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        $this->valueHolderbb9e4 = clone $this->valueHolderbb9e4;
    }

    public function __sleep()
    {
        $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, '__sleep', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;

        return array('valueHolderbb9e4');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerfe8b8 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerfe8b8;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerfe8b8 && ($this->initializerfe8b8->__invoke($valueHolderbb9e4, $this, 'initializeProxy', array(), $this->initializerfe8b8) || 1) && $this->valueHolderbb9e4 = $valueHolderbb9e4;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderbb9e4;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderbb9e4;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
