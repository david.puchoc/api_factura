<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211029020946 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE detalle (id INT AUTO_INCREMENT NOT NULL, factura_id INT DEFAULT NULL, id_producto INT DEFAULT NULL, producto VARCHAR(255) DEFAULT NULL, precio_unitario DOUBLE PRECISION DEFAULT NULL, cantidad INT DEFAULT NULL, precio_total DOUBLE PRECISION DEFAULT NULL, INDEX IDX_80397C30F04F795F (factura_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE detalle ADD CONSTRAINT FK_80397C30F04F795F FOREIGN KEY (factura_id) REFERENCES factura (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE detalle');
    }
}